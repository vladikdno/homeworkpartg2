package com.company;

import com.company.Model.Book;
import com.company.Model.Literature;
import com.company.Model.Magazine;
import com.company.Model.Yearbook;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        //Вызываем массив литератур, который у нас в классе литература
        Literature[] literatures = Literature.generateLiterature();

        Scanner in = new Scanner(System.in);
        System.out.println("Put year!");
        int inYear = Integer.parseInt(in.nextLine());

        boolean isFound = false;
        for (int i = 0; i < literatures.length; i++) {
            if (inYear == literatures[i].getPublishYear()){
                System.out.println(literatures[i]);
                isFound = true;
            }
//            else
//                System.out.println("Year Not found");
        }   if (!isFound) {
        }

        for (Literature literature : literatures) {
            if (literature instanceof Book) {
                System.out.println(String.format("Book name is: %s. And publish year: %d ", literature.getName(), literature.getPublishYear()));
            }
        }

    }
}
