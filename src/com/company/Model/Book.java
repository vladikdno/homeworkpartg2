package com.company.Model;

public class Book extends Literature {
    protected String author;
    protected String publisherBook;

    public Book(String name, int publishYear, String author, String publisherBook) {
        super(name, publishYear);
        this.author = author;
        this.publisherBook = publisherBook;
    }

    @Override
    public String toString() {
        return "Book: " + name + " " + publishYear + " " + author + " " + publisherBook;
    }
}
