package com.company.Model;

public class Literature {
    protected String name;
    protected int publishYear;

    public Literature(String name, int publishYear) {
        this.name = name;
        this.publishYear = publishYear;
    }

    public String getName() {
        return name;
    }

    public int getPublishYear() {
        return publishYear;
    }

    //создаем массив литературы и записываем туда книги, журналы и т.п.
    public static Literature[] generateLiterature() {
        Literature[] literatures = new Literature[4];

        literatures[0] = new Book("Заводной апельсин", 1962, "Энтони Берджис", "Heinemann ");
        literatures[1] = new Book("Заводной мандарин", 1971, "Энтони Берджис", "Heinemann ");
        literatures[2] = new Magazine("Forbes", 2016, "Most growth in the year", (byte) 6);
        literatures[3] = new Yearbook("Юелая книга Украины", 2018, "Оборонная политика Украины", "Министерство обороны Украины");

        return literatures;
    }
}
