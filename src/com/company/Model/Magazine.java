package com.company.Model;

public class Magazine extends Literature {
    protected String topicMagazine;
    protected byte month;

    public Magazine(String name, int publishYear, String topicMagazine, byte month) {
        super(name, publishYear);
        this.topicMagazine = topicMagazine;
        this.month = month;
    }

    @Override
    public String toString() {
        return "Magazine{" +
                "topicMagazine='" + topicMagazine + '\'' +
                ", month=" + month +
                ", name='" + name + '\'' +
                ", publishYear=" + publishYear +
                '}';
    }
}
