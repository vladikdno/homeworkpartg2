package com.company.Model;

public class Yearbook extends Literature {
    protected String topicYearbook;
    protected String publisherYearbook;

    public Yearbook(String name, int publishYear, String topicYearbook, String publisherYearbook) {
        super(name, publishYear);
        this.topicYearbook = topicYearbook;
        this.publisherYearbook = publisherYearbook;
    }

    @Override
    public String toString() {
        return "Yearbook{" +
                "topicYearbook='" + topicYearbook + '\'' +
                ", publisherYearbook='" + publisherYearbook + '\'' +
                ", name='" + name + '\'' +
                ", publishYear=" + publishYear;
    }
}
